# Contact

## Email

You can send an email to this domain, it doesn't matter what's before the @; I have a catch-all. This is probably the fastest way to establish contact with me, no guarantees though.

If you want to hide our conversations from prying eyes my fingerprint is:

```D22D 0210 9CB8 3DDF FF5C 3194 9DEB 9C8C 765E E14B```

For example, with gpg:

```gpg --recv-keys "D22D 0210 9CB8 3DDF FF5C 3194 9DEB 9C8C 765E E14B"```

## Social Media / Developer Sites

- [Github](https://github.com/necklaces)
- [Gitlab](https://gitlab.com/nsz)
- [BitBucket](https://bitbucket.org/Necklace/)
- [Twitter](https://twitter.com/nsz_no)
- [NotABug](https://notabug.org/necklace)
- [LinkedIn](https://no.linkedin.com/in/nichlas-severinsen-961b5a93)

## IRC

My handle on Freenode is 'necklace', but I'm not on much anymore. Feel free to send me a message if you see me. I have an fsf/member cloak so if you see someone with my name but without that cloak then either; I've stopped being a member, I've been hacked, or it's an impostor.
