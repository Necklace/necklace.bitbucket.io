# Ramblings

Welcome to my "blog"! This is where I practice writing, english, and the art of expressing oneself.

Feel free to [contact](/contact.html) me, or send me a [pull request](https://gitlab.com/nsz/nsz.gitlab.io) if you see grammatical errors, spelling mistakes, or sentences that don't make any sense.

## Posts

- [LVM / LUKS Tips & Tricks](/ramblings/lvm.html)
- [Liberate your music](/ramblings/liberate_your_music.html)
- [Why 'nsz'?](/ramblings/why_nsz.html)

## See also

- [gists](https://gist.github.com/Necklaces)
