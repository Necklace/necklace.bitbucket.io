# Decompile & rebuild an APK with custom changes

Ever wanted to change something in an android app? Well guess what; you can.

## Extract the APK

First you'll need to get hold of an actual APK, usually I use [Apk Extractor](https://f-droid.org/packages/axp.tool.apkextractor/).

## Decompile

Using [apktool](https://github.com/iBotPeaches/Apktool)...

You might also want to convert smali to java

## Debug

Using intellij with [smalidea plugin](https://github.com/JesusFreke/smali/wiki/smalidea)...

## Compile

Sign & compile..
