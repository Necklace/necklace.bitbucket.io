# Porting Unity Games to GNU/Linux

First written 2019-08-09.

*Note*: This is just a quick writeup, I might update this further sometime else.

*Note*: If you find this and share this, please use my nick 'Necklace' or this websites name 'nsz', rather than my real name. Thank you.

I recently found out that because Unity has a runtime for GNU/Linux, 
you can very simply copy the Unity game files from any other supported platform (eg. Windows or macOS),
and try to run the files "natively" under GNU/Linux.

This essentially means you can port games to GNU/Linux.

There are many caveats to it, but I'll walk you through how I ported Car Mechanic Simulator 2018 to GNU/Linux. 
At the time of writing there was no GNU/Linux build for that game, hopefully that'll change in the future.

A few notes about the walkthrough:

- Depending on the size of the game, this could be a long process.
- Without too much of a hassle, you can get the base game to run, but all the DLCs will be locked.
- Steam Achievements will not work, when given a working Steamworks library Steam will simply deny launching the game and error out with 'Invalid platform', or, if you have enabled Steam Play (read: Proton) for all titles Steam will simply launch the proton version.
- To get the DLCs to work you will need to do some reverse engineering of the Unity Mono .DLLs, this sounds difficult but really isn't.
- It might be possible to do the entire thing on GNU/Linux, but for convenience I ran a Windows VM for some things.

Let's jump in.

### 1. Acquire the files for the macOS version of Car Mechanic Simulator.

That's right, the macOS version.
When I first tried running the Windows version with the GNU/Linux runtime it gave me a rendering error as it tries to open the renderer for DirectX, which isn't
natively supported on GNU/Linux. You might be able to do some trickery using DXVK but I decided not to go that route, as I knew there was a macOS build, and surely
that would at least fallback to OpenGL since it supported OS X Mavericks - released before Metal was released. 
You might have to make such considerations for your desired game, if there's only a Windows build that doesn't fallback to OpenGL then it's game over.

There are two legal ways you can get hold of these files:

- Download CMS2018 using a Mac.
- Use the Steam commandline:

```
# Open steam://nav/console, either with xdg-open, or
# paste it into your browser, then run this command:
download_depot 645630 645633 5518517487118172910
```

(Exchange that last number with the newest [manifest](https://steamdb.info/depot/645633/manifests/))

### 2. Acquire the GNU/Linux Unity Runtime 

You will need the exact same version, the way we do this is we find the file called `level0` in the game files and do:

```
strings level0 | head -1
```

At the time of writing, this gave me:

> 2017.4.24f1

Now we know which version to acquire, but how do we get the runtime? There are several ways:

1. Download a full release, these are gigabytes in size:

- [Full releases](https://unity3d.com/get-unity/download/archive) (the ones with a X.Y.Zf[-∞] name format) 
- [Patched releases](https://unity3d.com/unity/qa/patch-releases) (the ones with a X.Y.Zp[1-∞] name format) 
- [Beta releases](https://unity3d.com/unity/beta/archive) (the ones with a X.Y.Zb[1-∞] name format) 

2. Comb your own game library for games with GNU/Linux builds on similar versions.

3. Download the Unity Editor release and make a GNU/Linux build, you don't have to have anything in the project, just make a build. This is what I went with, and I did so in a Windows VM, though that may not be necessary.

### 3. Edit the game file structure

Simply move the acquired runtime files into the game files folder like this:

```
├── something_Data 
│  ├── Mono 
│  │  ├── x86 
│  │  │  ├── libmono.so 
│  │  │  └── libMonoPosixHelper.so (since v5.5) 
│  │  └── x86_64 
│  │     ├── libmono.so 
│  │     └── libMonoPosixHelper.so (since v5.5) 
│  └── Plugins 
│     ├── x86 
│     │  └── ScreenSelector.so (since v4.3) 
│     └── x86_64 
│        └── ScreenSelector.so (since v4.3) 
├── gamename.x86 
└── gamename.x86_64 
```

At this point you're ready to try running gamename.platform, for CMS2018 that's:

```
./cms2018.x86_64
```

Or, since it's an executable, simply double click the file to execute it.

TODO: the rest.

TODO: check if [https://gitlab.com/Mr_Goldberg/goldberg_emulator](https://gitlab.com/Mr_Goldberg/goldberg_emulator) works

### Sources

- [https://www.gog.com/forum/general/running_nonlinux_unity3d_games_on_linux_and_natively/page1](https://www.gog.com/forum/general/running_nonlinux_unity3d_games_on_linux_and_natively/page1)
- [https://www.gamingonlinux.com/wiki/Unity_Games_Working_On_Linux_(User_Ported)](https://www.gamingonlinux.com/wiki/Unity_Games_Working_On_Linux_(User_Ported))















