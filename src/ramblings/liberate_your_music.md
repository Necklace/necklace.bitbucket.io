# Liberate your music

Originally written 2016-10-20, updated 2017-07-30. Last updated 2017-12-26.

## MP3 was proprietary

Before early 2017, the .mp3 (MPEG1-MPEG-2 Audio Layer III) file format was a proprietary, patented file format. Essentially, what this meant was that the patent owners could restrict developers from using the format in their software. This was bad.

The FSF launched a campaign called ["PlayOgg!"](https://playogg.org/) which aimed to spread the use and awareness of .ogg, as well as other formats, like Speex and FLAC, to store audio.

Early 2017 the patent on .mp3 expired. The FSF never really wrote about what this meant, but essentially it only lifted the hassle off of software writers to get a license to make software that could play .mp3 files.

## Okay..

Anyway, in response to the PlayOgg! campaign I (much later) wrote a script that would either use [youtube-dl](https://rg3.github.io/youtube-dl/) (a program that can download video and audio from a bunch of different sites, not just YouTube) to download audio from a site and convert it into Ogg Vorbis with some neat features like front-silence cut and dynamic audio normalization, using FFmpeg. It could also take directories and files as input.

The script was originally written in Bash, but in the end it grew so much that I decided to rewrite it in Python.

My original writeup went through the individual parts of the script and explained what they did and why. Previously I had the code just sitting here on my webpage, but now there's a git repository: [https://notabug.org/necklace/liberate](https://notabug.org/necklace/liberate)

