# Why nsz?

First written 2017-07-30. Last updated 2018-05-14.

I just think having a three-letter domain is more convenient than a firstname+lastname domain, so much so that I wish I had a middle name. That way all three characters could make sense. The Z in NSZ is a throwaway character, I saw that the nsz.no domain was not taken and bought it (you can't buy two-letter domains under the .no TLD). I realized afterwards that NSZ was/is some polish military organization. This website has nothing to do with them. Neither does it have anything to do with the Nanking Safety Zone.

