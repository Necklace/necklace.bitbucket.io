# LVM / LUKS Tips & Tricks

2018-06-12

aka LVM / LUKS commands I keep forgetting

## Renaming a volume:

Sometimes I do a `dd` backup of a drive and end up having two drives with the same name. This makes one drive un-mountable if the other drive is mounted. To fix this we have to rename one of the volume groups, first we'll figure out what drive we want to rename, do a:

```
# blkid
```

Then, under one of the /dev/mapper listings, find the UUID of the volume group you want to rename. You can also do a `lsblk` and figure out which drive is mapped to which volume.Afterwards, we just have to rename the drive:

```# vgrename old-vg-id new-name
```

For example:

```
# vgrename 746895d5-ea6e-48ae-b1f7-7db7c5d14378 myNewVG
```
