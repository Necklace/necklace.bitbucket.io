# Nichlas Severinsen

[Contact](/contact.html)

## Game Programmer / Software Engineer

Highly interested in everything that has to do with computers. Passionate with a professional "make it work"-attitude. Independent and quick learner, absolutely capable of working in teams. I love problem solving and urge to learn new things every day.

## Technical Skills

### Programming Languages

| Language   | Self-rating |            |
| Python     | ★★★★☆       | Proficient |
| SQL        | ★★★★☆       | Proficient |
| C++        | ★★★★☆       | Proficient |
| PHP        | ★★★★☆       | Proficient |
| C          | ★★★★☆       | Proficient |
| LaTeX      | ★★★☆☆       | Competent  |
| R          | ★★★☆☆       | Competent  |
| Java       | ★★★☆☆       | Competent  |
| JavaScript | ★★☆☆☆       | Beginner   |
| C#         | ★★☆☆☆       | Beginner   |
| Go         | ★☆☆☆☆       | Novice     |
| D          | ★☆☆☆☆       | Novice     |

### Keywords

- Databases: MySQL, Mariadb, PostgreSQL, SQLite, Sphinxsearch
- Operating Systems: GNU/Linux
- Networking: HTTP, HTTPS (letsencrypt), REST, TCP, UDP, RDP, SSH, IMAP
- Formats: XML, HTML/CSS, JSON, CSV/XLS(X), Markdown
- Version Control: Git, SVN
- Development & Collaboration: SCRUM, Kanban, Continous Integration, Automated Testing
- Services: Amazon AWS (S3, EC2, Route53, IAM, RDS), VPS, Git hosts
- Games: AR, VR, OpenGL, SDL2, SFML, Irrlicht, Unity, Blender

## Professional Experience

### Developer, Full time. 2018-04 to Present - Arkivverket, Oslo, Norway

- Employed as a software developer in the "Forskning og Utvikling" (Research and Development) section.
- PHP7 & 5.6, JavaScript, React, Node.js, jpg2000, SQL, Sphinxsearch, SFTP, Mako PHP framework

### Sole Proprietorship. 2017-06 to Present - NSZ Nichlas Severinsen, Oslo, Norway

- Python, R, SQL, AWS, GNU/Linux, Installation, Maintenance, etc. for various clients.
- Data Manager, Full time. 2016-05 to 2018-03 - Storekeeper AS, Sandvika, Norway

### Data Manager, Full time. 2016-05 to 2018-03 - Storekeeper AS, Sandvika, Norway

- Development of their new website & customer portals/dashboards using R and Shiny.
- Installation, setup, and maintenance of their GNU/Linux & Windows servers.
- Automation of ELT and/or ETL, often combined with scraping.
- Database administration, data preparation, automation, quality assurance of data, etc.

### Data Manager, Part time. 2015-02 to 2016-05 - Storekeeper AS, Oslo, Norway

- Installation, setup, and maintenance of their GNU/Linux & Windows servers.
- Automation of ELT and/or ETL, often combined with scraping.
- Database administration, data preparation, automation, quality assurance of data, etc.

## Education

### Bachelor in Game Programming. 2013-08 to 2016-05 - NTNU, Gjøvik, Norway

- [NTNU - Norwegian University of Science and Technology](https://www.ntnu.edu/gjovik)

Bachelor thesis: [VRterra](https://brage.bibsys.no/xmlui/handle/11250/2407260)

- IMT2021 - Algorithmic Methods
- IMT3591 - Artificial Intelligence
- IMT3912 - Bachelor
- IMT2431 - Data Communications and Network Security
- IMT2571 - Datamodeling and Databases
- IMT1031 - Fundamental Programming
- IMT1361 - Game Design
- IMT3601 - Game Programming
- IMT2531 - Graphic Programming
- REA1101 - Mathematics for Computer Science
- REA2061 - Mathematics for Game Programming
- IMT3662 - Mobile Development Theory
- IMT3801 - Multi-threaded Programming
- IMT1082 - Object-Oriented Programming
- IMT2282 - Operating Systems
- IMT3602 - Professional Programming
- IMT2581 - Rapid Prototyping and Innovation
- IMT3281 - Software Development
- IMT2243 - Software Engineering

## Contributions and Projects

Contributions I've tried to make towards [FLOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software)

- [Contributed to the Mako PHP framework](https://github.com/mako-framework/framework/commits?author=Necklaces) and its [documentation](https://github.com/mako-framework/docs/commits?author=Necklaces)
- [Made a FLOSS (free, libre, open source software) application for decrypting PS3 games in Python](https://notabug.org/necklace/libray)
- [Added features to a cross platform C++ desktop app written with Qt called Signet](https://github.com/nthdimtech/signet-desktop-client/commit/b2f7e0d768a94cb99d205b653e6755d5306900a6)
- [Contributed features and syntax-highlighting to a terminal text editor written in Go called Micro](https://github.com/zyedidia/micro/commits?author=Necklaces)
- [Wrote a tool in Python to archive/restore emails via IMAP](https://gitlab.com/nsz/imaparchiver)
- [Wrote a program in C to convert number systems (for example decimal to binary)](https://gitlab.com/nsz/clinsc)
- [Wrote a tool in Python to automatically convert music from videos/audio/web to OGG Vorbis](https://notabug.org/necklace/liberate)
- [Hacked together a shell script in Bash to start R/Shiny programs from commandline](https://gitlab.com/nsz/ssfc)
- [Did some scraping using Python to get data I could analyze with R, made some cloropleth plots and animated gifs.](https://gitlab.com/nsz/EyeStats)
- [Fixed some bugs in a Python wrapper for SFML, called python-sfml](https://github.com/Sonkun/python-sfml/pulls/Necklaces)
- [Tried to resurrect a 10 year old PHP project from the dead](https://notabug.org/necklace/online-bookmarks-2)
- [Made a script in Python to remove blank pages in PDFs](https://gitlab.com/nsz/pydf-rm-blnx)
