# Collections, Lists, Etc.

I like to collect stuff. So here, have some stuff.

## Libre News

- [Freepost](https://freepo.st/)
- [FSF News](https://www.fsf.org/news)
- [WINEHQ](https://www.winehq.org/news/)
- [Replicant](https://blog.replicant.us/)
- [Linux-libre](https://www.fsfla.org/ikiwiki/selibre/linux-libre/)

## News

- [EFF](https://www.eff.org/)
- [LXer](http://lxer.com/)
- [LWM.net](https://lwn.net/)
- [Arch News](https://www.archlinux.org/news/)
- [Bleeping Computer](https://www.bleepingcomputer.com/)
- [N O D E](https://n-o-d-e.net/)
- [cnx-software](https://www.cnx-software.com/)
- [TorrentFreak](https://torrentfreak.com/)
- [LinuxJournal](https://www.linuxjournal.com/)
- [Kernel](https://www.kernel.org/)

## Libre Forums

- [Trisquel GNU/Linux Forums](https://trisquel.info/en/forum/trisquel-users)

## Programming

- [R-bloggers](https://www.r-bloggers.com/)
- [The Hitchiker's Guide to Python](http://docs.python-guide.org/en/latest/)
- [/r/programming](https://www.reddit.com/r/programming/)
- [How to write bad code](https://madaan.github.io/wbc/)

## People

- [Petter Reinholdtsen](http://people.skolelinux.org/pere/blog/)
- [Bruce Schneier](https://www.schneier.com/)
- [Brian Krebs](https://krebsonsecurity.com/)
- [Jason Self](https://jxself.org/archive.shtml)

## Iridium Extensions

- [uBlock Origin](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm)
- [HTTPS Everywhere](https://chrome.google.com/webstore/detail/https-everywhere/gcbommkclmclpchllfjekcdonpmejbdp/related)
- [Quick Javascript Switcher](https://chrome.google.com/webstore/detail/quick-javascript-switcher/geddoclleiomckbhadiaipdggiiccfje/related)
- [User-Agent Switcher](https://chrome.google.com/webstore/detail/user-agent-switcher/bhchdcejhohfmigjafbampogmaanbfkg/related) (with [this](https://techblog.willshouse.com/2012/01/03/most-common-user-agents/))
- [YARC](https://chrome.google.com/webstore/detail/yet-another-rest-client/ehafadccdcdedbhcbddihehiodgcddpl)

## Arch

- [Arch Official](https://www.archlinux.org/)
- [Arch 32-bit](https://archlinux32.org/)
- [Arch ARM](https://archlinuxarm.org/)
- [Arch Hurd](https://archhurd.org/)

## Page Scanners

- [https://securityheaders.io](https://securityheaders.io)
- [https://tls.imirhil.fr/](https://tls.imirhil.fr/)
- [https://sitecheck.sucuri.net/](https://sitecheck.sucuri.net/)
- [https://search.google.com/test/mobile-friendly](https://search.google.com/test/mobile-friendly)
- [https://developers.google.com/speed/pagespeed/insights/](https://developers.google.com/speed/pagespeed/insights/)
- [https://internet.nl/test-site/](https://internet.nl/test-site/)
- [https://webspeedtest.cloudinary.com](https://webspeedtest.cloudinary.com)
- [https://sonarwhal.com](https://sonarwhal.com)
- [https://www.ssllabs.com](https://www.ssllabs.com)
- [https://www.sslshopper.com/ssl-checker.html](https://www.sslshopper.com/ssl-checker.html)
- [http://nibbler.silktide.com/en](http://nibbler.silktide.com/en)

## Phone Stuff

- [postmarketOS](https://postmarketos.org/)
- [Librem 5](https://puri.sm/shop/librem-5/)
- [LineageOS](https://lineageos.org/)
- [Replicant](https://replicant.us/)
- [List on Wikipedia](https://en.wikipedia.org/wiki/List_of_open-source_mobile_phones)

## Public Transport

- [mon.ruter.no](https://mon.ruter.no/monitor/2190400/Sandvika)

## Translation & Romanization

- [Hangul](http://www.kawa.net/works/ajax/romanize/hangul-e.html)
- [Kana](http://romaji.me/)
- [Apertium - A FLOSS translation platform](https://www.apertium.org/)

## Homebrew Development

- [PSP](https://en.wikibooks.org/wiki/PSP_Programming)
- [3DS](http://wiki.gbatemp.net/wiki/3DS_Homebrew_Development)

## Tools

- [Hex to file](http://tomeko.net/online_tools/hex_to_file.php?lang=en)
- [(Libre?) image share](https://framapic.org/#)

## Misc. links I always struggle to find

- [강경민](https://www.youtube.com/channel/UCqDCXaJBTnXG4i_3Z98V-bw)

