# Onions - Opinions without π

Here are my onions. Often bitter, sometimes sour, occasionally rotten. Also known as rants. Consider yourself warned.

## Onions

- [Windows sucks](/onions/worst_os_ever.html)
- [Unsupported companies](/onions/unsupported_companies.html)

## See also

- [https://wiki.theory.org/index.php/YourLanguageSucks](https://wiki.theory.org/index.php/YourLanguageSucks)
