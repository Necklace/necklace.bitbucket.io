# Unsupported Companies

Companies that I simply don't support. Some of the reasons here may be a little on the ranty/vengeful side, consider yourself warned.

## Apple

todo

- [See also](https://www.gnu.org/proprietary/malware-apple.html)

## Das Keyboard

I've use their Model S (EU, Silent) keyboard since like 2009 I think, so over 10 years. Unfortunately, they've decided to ban people mentioning FLOSS firmware alternative to their latest models, so in return I'll no longer be supporting them.

- [Banned source (twitter)](https://twitter.com/sebirdman/status/1026625633953013761) [(archive)](https://web.archive.org/web/20180808144843/https:/twitter.com/sebirdman/status/1026625633953013761)

## Facebook

- [For just existing](https://veekaybee.github.io/2017/02/01/facebook-is-collecting-this/) [(archive)](https://web.archive.org/web/20180626121937/https://veekaybee.github.io/2017/02/01/facebook-is-collecting-this/)

## Google

todo

- [See also](https://www.gnu.org/proprietary/malware-google.html)

## Microsoft

todo

- [See also](https://www.gnu.org/proprietary/malware-microsoft.html)
