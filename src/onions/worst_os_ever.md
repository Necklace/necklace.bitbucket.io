## Windows Sucks

Windows..

- is bloated & unoptimalized
- has slower boot time
- has buggy ui
- [has inconsistent ui](https://pics.me.me/select-all-squares-with-inconsistent-ul-will-qu-tly-move-56888147.png) [(archive)](https://web.archive.org/web/20190616105808/https://pics.me.me/select-all-squares-with-inconsistent-ul-will-qu-tly-move-56888147.png)
- has forced updates
- has a huge install size
- has slower install time
- no way to (directly) fix bugs
- privacy issues
- has slower update time
- [is overall slower](http://www.bitsnbites.eu/benchmarking-os-primitives/) [(archive)](https://web.archive.org/web/20180626090341/http://www.bitsnbites.eu/benchmarking-os-primitives/)
- has overall more vulnerabilities
- collects a shitload of telemetry by default, usually not even disable-able.

Microsoft has..

- [repeatedly](https://www.theregister.co.uk/2017/04/19/chap_fixes_microsofts_windows_7_and_8_update_block_on_new_cpus/) [(archive)](https://archive.fo/IQkxq)
- [kicked](https://kotaku.com/xbox-one-needs-to-connect-to-the-internet-every-24-hour-511751949) [(archive)](https://web.archive.org/web/20180306110025/https://kotaku.com/xbox-one-needs-to-connect-to-the-internet-every-24-hour-511751949)
- [customers](https://www.pcmag.com/news/343984/free-microsoft-onedrive-storage-drops-to-5gb-in-july) [(archive)](https://web.archive.org/web/20171019011014/https://www.pcmag.com/news/343984/free-microsoft-onedrive-storage-drops-to-5gb-in-july)
- [in](https://www.addictivetips.com/windows-tips/you-need-to-stop-kb-3035583-from-installing-again/) [(archive)](https://web.archive.org/web/20180525233926/https://www.addictivetips.com/windows-tips/you-need-to-stop-kb-3035583-from-installing-again/)
- [the](https://www.addictivetips.com/windows-tips/you-need-to-stop-kb-3035583-from-installing-again/) [(archive)](https://web.archive.org/web/20180327040709/https://www.rockpapershotgun.com/2015/07/30/windows-10-privacy-settings/)
- [nuts](https://www.cnet.com/news/microsoft-confirms-windows-adheres-to-broadcast-flag/) [(archive)](https://web.archive.org/web/20180622202015/https://www.cnet.com/news/microsoft-confirms-windows-adheres-to-broadcast-flag/)

But they also have..

- [A backdoor to install unwanted apps](https://www.ghacks.net/2018/07/02/windows-10-installing-unwanted-store-apps/) [(archive)](https://archive.fo/Cswpl)
- [Vista and XP had similar](https://www.informationweek.com/microsoft-updates-windows-without-user-permission-apologizes/d/d-id/1059183) [(archive)](https://web.archive.org/web/20190421070301/https://www.informationweek.com/microsoft-updates-windows-without-user-permission-apologizes/d/d-id/1059183)
- [Colluded with the NSA](http://techrights.org/wiki/index.php/Microsoft_and_the_NSA) [(archive)](https://web.archive.org/web/20190524224336/http://techrights.org/wiki/index.php/Microsoft_and_the_NSA)

See also:

- [Microsoft's Software is Malware](https://www.gnu.org/proprietary/malware-microsoft.html) [(archive)](https://web.archive.org/web/20190616045837/https://www.gnu.org/proprietary/malware-microsoft.html)
