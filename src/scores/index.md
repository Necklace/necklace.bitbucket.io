# Scores

I like music, here are some scores I've made/copied from sheets I had. If you've looked in the repository there are more than what I've listed here, but I consider the unlisted ones unfinished.

## Scores:

- [Kyrie Eleison](/scores/Kyrie_Eleison.mscz)
- [Velsignelsen (Blessing)](/scores/Velsignelsen.mscz)
- [Laudate Omnes Gentes](/scores/Laudate_Omnes_Gentes.mscz)
- [Bruremarsj (Bridesmarch)](/scores/Bruremarsj.mscz)
