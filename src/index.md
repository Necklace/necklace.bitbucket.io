# NSZ Nichlas Severinsen

## About

This is the homepage of NSZ Nichlas Severinsen (org. no. 919 193 743), a sole proprietorship run by me - Nichlas Severinsen.

## Hire me

My skills include:

- C
- R
- C++
- Bash
- LaTeX
- Python
- GNU/Linux
- PostgreSQL
- MySQL / Mariadb

I have a bachelor's degree in [game programming](https://en.wikipedia.org/wiki/Game_programming) from [NTNU - Norwegian University of Science and Technology](https://www.ntnu.edu/gjovik). The thesis was written with [Mårten Nordheim](http://morten242.com/) and can be read [here](https://brage.bibsys.no/xmlui/handle/11250/2407260).

[My contact information can be found here](/contact.html).

[Full CV / Online resume can be found here](/resume.html).

## Other

This website also hosts [my personal "blog"](/ramblings/).

## Links to things I like

People:

- [Mårten Nordheim](http://morten242.com/)
- [Eirik Igland](http://eirabben.com/)

- [Richard Stallman](https://stallman.org/)
- [Edward Snowden](https://edwardsnowden.com/)
- [Eric S. Raymond](http://www.catb.org/esr/)
- [Dr. Jordan B. Peterson](https://jordanbpeterson.com/)

Organizations:

- [Free Software Foundation](https://fsf.org/)
- [GNU](https://gnu.org/)
- [The Tor Project, Inc.](https://www.torproject.org/)
- [Pirate Parties International](https://pp-international.net/)

## Banners

![Free Software Foundation](https://static.fsf.org/nosvn/associate/crm/1315308.png?31536000) ![EFF Blue Ribbon](https://www.eff.org/files/brstrip.gif)


