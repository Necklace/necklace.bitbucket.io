#!/usr/bin/env python3
# -*- coding: utf8 -*-


import re
import os
import ui
import sys
import glob
import pathlib
import argparse
from datetime import datetime


def mdlink_to_html(txt):
	"""Find markdown links in string and convert to html"""

	# Anything that isn't a square closing bracket
	name_regex = '[^]]+'
	# http:// or https:// followed by anything but a closing paren
	url_regex = r'(http[s]?://|\/)[^)]+'

	markup_regex = r'!?\[({0})]\(\s*({1})\s*\)'.format(name_regex, url_regex)

	out = txt

	for x in re.finditer(markup_regex, txt):
		match = ''.join(x.group())
		if match[0] == '!':
			out = out.replace(match, ui.a({'href': x.group(2), 'class': 'nostyle'}, ui.img({'src': x.group(2), 'alt': x.group(1), 'class': 'imgstyle'})))
		else:
			out = out.replace(match, ui.a({'href': x.group(2)}, x.group(1)))

	return out


def mdic_to_html(txt):
	"""Find inline codes in string and convert to html"""

	out = txt

	for x in re.finditer('`([^`]+)`', txt):
		match = ''.join(x.group())
		out = out.replace(match, ui.ic(x.group(1)))
	
	return out


def mdcode_to_html(txt):
	"""Find multiline codes in string and convert to html"""

	out = txt
	for x in re.finditer(r'```\n?(.*?)```', txt, re.DOTALL):
		match = ''.join(x.group())
		out = out.replace(match, ui.code(x.group(1).replace('\n',ui.br()) ))

	return out


def minimize_css(lines):
	"""Minimize CSS"""

	# Remove newlines

	out = ''.join([line.replace('\n','').strip() for line in lines])

	# Remove redundant whitespace

	out = ' '.join(out.split())

	# Remove some other redundance

	out = out.replace(' {','{').replace(': ',':').replace(', ',',').replace(';}','}').replace('0.','.').replace('\'','').replace(' !','!')

	# Remove /* comments */

	for x in re.finditer(r'(\/\*.*?\*\/)', out, re.DOTALL):
		match = ''.join(x.group())
		out = out.replace(match, '')

	# Remove any remaining empty tags

	for x in re.finditer(r'(\w+{})', out):
		match = ''.join(x.group())
		out = out.replace(match, '')

	return out



if __name__ == '__main__':

	parser = argparse.ArgumentParser(description='Recursively generate .html from .md')
	parser.add_argument('-m', '--markdown', dest='markdown', default='src', help='Path to folder containing markdown files.')
	parser.add_argument('-w', '--webdir', dest='webdir', default='www', help='Path to folder where HTML files should be placed.')
	parser.add_argument('-s', '--style', dest='style', default='style.css', help='Path to style.css file.')
	parser.add_argument('-l', '--language', dest='language', default='en-US', help='Language.')
	args = parser.parse_args()

	if not os.path.isdir(args.markdown):
		print('Missing folder \'%s\' with markdown files.' % args.markdown)
		sys.exit()

	with open(args.style, 'r') as stylein:
		with open(args.webdir + '/min.style.css', 'w+') as styleout:
			styleout.write(minimize_css(stylein.readlines()))

	# Traverse all files

	for infile in glob.iglob( args.markdown + '/**/*.md', recursive=True):

		outfile = infile.replace(args.markdown, args.webdir).replace('.md', '.html')

		# Make sure output subfolders exist

		if not os.path.exists(outfile):
			os.makedirs('/'.join(outfile.split('/')[:-1]), exist_ok=True)

		with open(infile, 'r') as md:
			with open(outfile, 'w+') as html:
				incontent = [line.strip() for line in md.readlines()]

				title = ' | '.join(['nsz'] + infile.replace('.md','').split('/')[1:])
				url = '/'.join(['nsz.no'] + infile.replace('.md','').split('/')[1:])

				# Hackily parse markdown

				# First we'll take care of multi-line code chunks, since they're a pain in the butt

				incontent = mdcode_to_html('\n'.join(incontent)).split('\n')

				# Then we'll hackily parse the rest with a loop

				content = ''
				listing = []
				table = []
				tabling = False
				paragraph = ''
				paragraphing = False
				for line in incontent:
					if not line:
						if listing != []:
							content += ui.ul(listing)
							listing = []
						if tabling == True and table != []:
							content += ui.table(table[1:], headers = table[0], attrs = {'class': 'centered'})
							tabling = False
						if paragraphing == True and paragraph:
							content += ui.p(paragraph.strip())
							paragraph = ''
							paragraphing = False
						continue

					spacesplit = line.split(' ')
					if spacesplit[0] == '#':
						content += ui.h1({'class': 'center'}, ' '.join(spacesplit[1:]))
					elif spacesplit[0] == '##':
						content += ui.h2(' '.join(spacesplit[1:]))
					elif spacesplit[0] == '###':
						content += ui.h3(' '.join(spacesplit[1:]))	
					elif spacesplit[0] == '-':
						listing.append(mdlink_to_html(' '.join(spacesplit[1:])))
					elif spacesplit[0] == '|':
						table.append([column.strip() for column in line.split('|')][1:-1])
						tabling = True
					elif spacesplit[0] == '>':
						content += ui.blockquote(line[2:])
					else:
						if listing != []:
							content += ui.ul(listing)
							listing = []
							continue
						if tabling == True and table != []:
							content += ui.table(table[1:], headers = table[0], attrs = {'class': 'centered'})
							tabling = False
							continue
						if line != '\n':
							paragraph += mdic_to_html(mdlink_to_html(line)) + ' '
							paragraphing = True

				if paragraph:
					content += ui.p(paragraph.strip())

				if listing != []:
					content += ui.ul(listing)
					listing = []

				# Write HTML

				outhtml = ui.html({'lang': args.language},

					# HTML Head (metadata)

					ui.head(

						# Meta tags & icons

						ui.container([
							ui.meta({'charset': 'UTF-8'}),
							ui.meta({'name': 'viewport', 'content': 'width=device-width, initial-scale=1' }),
							ui.meta({'property': 'og:title', 'content': title}),
							ui.meta({'property': 'og:url', 'content': url}),
							ui.meta({'property': 'og:image', 'content': 'nsz.no/apple-touch-icon.png'}),
							ui.link({'rel': 'author', 'href': '/humans.txt'}),
							ui.link({'rel': 'icon', 'href': '/favicon.ico?31536000'}),
							ui.link({'rel': 'apple-touch-icon', 'href': '/apple-touch-icon.png'})
						]),

						# Title

						ui.title(title),

						# Style

						ui.link({
							'rel': 'stylesheet',
							'type': 'text/css; charset=utf-8',
							'href': '/min.style.css',
							'integrity': ui.integrity(args.webdir + '/min.style.css'),
							'crossorigin': 'anonymous'
						})
					),

					# HTML Body (content)

					ui.body(

						# Navigation

						ui.nav({'class': 'center'},
							ui.div({'class': 'divider'}, ' '),
							ui.a({'href': '/', 'class': 'nav'}, 'nsz.no'), ' ',
							ui.a({'href': '/resume.html', 'class': 'nav'}, 'cv'), ' ',
							ui.a({'href': '/contact.html', 'class': 'nav'}, 'contact'), ' ',
							ui.a({'href': '/ramblings/', 'class': 'nav'}, 'blog'), ' ',
							ui.div({'class': 'divider'}, ' ')
						),

						# Content

						ui.main(
							content
						),

						# Footer

						ui.div(
							ui.div({'class': 'divider'}, ' '),
							ui.p('Copyright © 2016 - ' + str(datetime.now().year) + ' Nichlas Severinsen')
						)
					)
				)

				html.write('<!DOCTYPE html>')
				html.write(outhtml)







