#!/bin/env python3


import csv
import sys
import requests
from bs4 import BeautifulSoup


if __name__ == "__main__":

  with open("all_combinable.txt", "r") as f:
    data = f.readlines()

  url = "http://dragonsdogma.wikia.com/wiki/"
  with open('combinables.csv', 'w') as f:
    w = csv.DictWriter(f, ['Name', 'Base Weight', 'Cost', 'Value', 'Type', 'Forgery Cost', 'Link'])
    w.writeheader()
    for item in data:
      info = {}
      info['Name'] = item.strip()
      link = url + info['Name'].replace(' ','_')
      info['Link'] = link
  
      r = requests.get(link)
      soup = BeautifulSoup(r.text)

      class_list = "pi-item pi-data pi-item-spacing pi-border-color"
      for attr in [x.text.strip() for x in soup.find_all('div', class_ = class_list )]:
        try:
          name, value = attr.split('\n')
          info[name] = value.replace('G','').strip().replace(',','.')
        except ValueError:
          print(attr)

      w.writerow(info)


