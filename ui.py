#!/usr/bin/env python3
# -*- coding: utf8 -*-

import sys
import base64
import hashlib
import collections
from datetime import datetime

def elem(*args, name):
	C = collections.Counter()
	elems = []
	for arg in args:
		if isinstance(arg, dict):
			C.update(arg)
		else:
			elems.append(arg)
	ret_str = "<"+name
	for key, val in dict(C).items():
		ret_str += ' '+key+'='+'"'+val+'"'
	ret_str += ">"
	if elems:
		for elem in elems:
			if elem:
				ret_str += elem
		ret_str += "</"+name+">"
	return ret_str


def elemx(name):
	return lambda *args: elem(*args, name=name)


def container(elements):
	ret_str = ""
	for element in elements:
		ret_str += element
	return ret_str


a = elemx('a')
b = elemx('b')
i = elemx('i')
p = elemx('p')
br = elemx('br')
h1 = elemx('h1')
h2 = elemx('h2')
h3 = elemx('h3')
h4 = elemx('h4')
h5 = elemx('h5')
h6 = elemx('h6')
li = elemx('li')
td = elemx('td')
th = elemx('th')
tr = elemx('tr')
div = elemx('div')
img = elemx('img')
nav = elemx('nav')
code = elemx('code')
body = elemx('body')
head = elemx('head')
html = elemx('html')
link = elemx('link')
meta = elemx('meta')
main = elemx('main')
title = elemx('title')
quote = elemx('quote')
blockquote = elemx('blockquote')


def ul(row_list, attrs={}):
	ret_str = ""
	for row in row_list:
		ret_str += li(row)
	return elem(ret_str, attrs, name='ul')


def table(content, headers=None, attrs={}):
	head_str = ""
	if headers:
		for header in headers:
			head_str += th(header)
		head_str = tr(head_str)
	data_str = ""
	for row in content:
		col_str = ""
		for col in row:
			col_str += td(col)
		data_str += tr(col_str)
	return elem(head_str+data_str, attrs, name='table')


def to_file(filename,ui):
	with open(filename, 'w') as outfile:
		outfile.write('<!DOCTYPE html>')
		outfile.write(ui)


def integrity(filename, block_size=65536):
	with open(filename, 'rb') as f:
		ret = "sha256-" + base64.b64encode(hashlib.sha256(f.read()).digest()).decode('utf8')
	with open(filename, 'rb') as f:
		ret += " sha512-" + base64.b64encode(hashlib.sha512(f.read()).digest()).decode('utf8')
	return ret


def archive(url):
	return a({'href': url}, '(archive)')


def ic(txt):
	"""Inline Code"""
	return code({'class': 'inline'}, txt)



